# layui-dropdown

请使用 Layui 官方【下拉菜单】组件

---

## 效果

https://hezulong1.gitee.io/layui-dropdown

## 开发

``` bash
npm i # 安装依赖

npm run dev # 编译开发文件

npm run build # 编译生产文件

npm run watch # 开发模式下编译
```

see: [localhost:3000](localhost:3000)
